<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'blogs' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.$+<=;EngcdaQG]f`Q,c2(kngVV.Hlm8st^TVz}dw:HWX5oPdnm$4H|x88HP1EcJ' );
define( 'SECURE_AUTH_KEY',  '#&uY.R5@T|A5R$.+OSI8rgrw*i98&L~e}pxK]3?csjfTUmXzX`Q:G(W>s}5`1_E6' );
define( 'LOGGED_IN_KEY',    '$M9*ylb)+-Ne>)9X0:XiusV_h={C;2[Osaw+$,nR{+M;w~O9t}c<O$~V_CkOS[20' );
define( 'NONCE_KEY',        '5gkLqjZ{KZXnT?+D7$P5b%o4_}FSV#O7D@s{#fwKStH?7W%:x8LI-M^HIGb9aUeh' );
define( 'AUTH_SALT',        'vQO=G.^~&@a!]%]sz*aXV0h^A+Zwc`V!5OqG2w;HrRW{G>[FMr H&do{gIeM?w=R' );
define( 'SECURE_AUTH_SALT', '<x<qctV Xb<0&]%A-a?fXq+2:X$#w$k8w+~WoylW]LK&qDu-gtF|F,mSx6!as3S}' );
define( 'LOGGED_IN_SALT',   'w7W_;^q4%!r:{T#%D##&L_w-2 GkR$)p(WP&_f.i!eMPd!edO.xfu$`X_Qzm[o=<' );
define( 'NONCE_SALT',       '$#![H.,)IO<(y_g`2/(K1Xf9 PYt8TO`a/]HPjXQDklnD+MNgIR6jD[~YM=/5Q7h' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
